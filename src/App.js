import { ThemeProvider } from "styled-components";

import { BrowserRouter, Route } from "react-router-dom";
import Home from "./screens/home";
import Impressum from "./screens/impressum";
import Datenschutz from "./screens/datenschutz";
import theme from "./utility/theme";
import "react-multi-carousel/lib/styles.css";

function App() {
  return (
    <ThemeProvider theme={theme}>
      <BrowserRouter>
        <Route component={Home} exact path="/" />
        <Route component={Impressum} exact path="/impressum" />
        <Route component={Datenschutz} exact path="/datenschutz" />
      </BrowserRouter>
    </ThemeProvider>
  );
}

export default App;
