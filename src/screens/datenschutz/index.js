import React, { useState } from "react";

import styled from "styled-components";
import Layout from "../layout/index";

const Datenschutz = () => {
  return (
    <Layout>
      <div
        style={{
          paddingLeft: "10%",
          paddingRight: "10%",
          marginBottom: 10,
        }}
      >
        <p>
          <strong>Datenschutzerkl&auml;rung</strong>
          <br />
          <strong>Allgemeiner Hinweis und Pflichtinformationen</strong>
          <br />
          <strong>Benennung der verantwortlichen Stelle</strong>
          <br />
          Die verantwortliche Stelle f&uuml;r die Datenverarbeitung auf dieser
          Website ist:
          <br />
          Fedaris GbR
          <br />
          Geschäftsführer: Ahmad-Ismail Tahir
          <br />
          <p>Werrestra&szlig;e 53</p>
          <p>32049 Herford</p>
          <br />
          <br />
          <br />
          Die verantwortliche Stelle entscheidet allein oder gemeinsam mit
          anderen &uuml;ber die Zwecke und Mittel der Verarbeitung von
          personenbezogenen Daten (z.B. Namen, Kontaktdaten o. &Auml;.).
          <br />
          Widerruf Ihrer Einwilligung zur Datenverarbeitun
          <br />
          Nur mit Ihrer ausdr&uuml;cklichen Einwilligung sind einige
          Vorg&auml;nge der Datenverarbeitung m&ouml;glich. Ein Widerruf Ihrer
          bereits erteilten Einwilligung ist jederzeit m&ouml;glich. F&uuml;r
          den Widerruf gen&uuml;gt eine formlose Mitteilung per E-Mail. Die
          Rechtm&auml;&szlig;igkeit der bis zum Widerruf erfolgten
          Datenverarbeitung bleibt vom Widerruf unber&uuml;hrt.
          <br />
          <strong>
            Recht auf Beschwerde bei der zust&auml;ndigen Aufsichtsbeh&ouml;rde
          </strong>
          <br />
          Als Betroffener steht Ihnen im Falle eines datenschutzrechtlichen
          Versto&szlig;es ein Beschwerderecht bei der zust&auml;ndigen
          Aufsichtsbeh&ouml;rde zu. Zust&auml;ndige Aufsichtsbeh&ouml;rde
          bez&uuml;glich datenschutzrechtlicher Fragen ist der
          Landesdatenschutzbeauftragte des Bundeslandes, in dem sich der Sitz
          unseres Unternehmens befindet. Der folgende Link stellt eine Liste der
          Datenschutzbeauftragten sowie deren Kontaktdaten bereit:&nbsp;
          <a
            href="https://www.bfdi.bund.de/DE/Infothek/Anschriften_Links/anschriften_links-node.html"
            target="_blank"
            rel="noreferrer noopener"
          >
            https://www.bfdi.bund.de/DE/Infothek/Anschriften_Links/anschriften_links-node.html
          </a>
          .<br />
          <strong>Recht auf Daten&uuml;bertragbarkeit</strong>
          <br />
          Ihnen steht das Recht zu, Daten, die wir auf Grundlage Ihrer
          Einwilligung oder in Erf&uuml;llung eines Vertrags automatisiert
          verarbeiten, an sich oder an Dritte aush&auml;ndigen zu lassen. Die
          Bereitstellung erfolgt in einem maschinenlesbaren Format. Sofern Sie
          die direkte &Uuml;bertragung der Daten an einen anderen
          Verantwortlichen verlangen, erfolgt dies nur, soweit es technisch
          machbar ist.
          <br />
          <strong>
            Recht auf Auskunft, Berichtigung, Sperrung, L&ouml;schung
          </strong>
          <br />
          Sie haben jederzeit im Rahmen der geltenden gesetzlichen Bestimmungen
          das Recht auf unentgeltliche Auskunft &uuml;ber Ihre gespeicherten
          personenbezogenen Daten, Herkunft der Daten, deren Empf&auml;nger und
          den Zweck der Datenverarbeitung und ggf. ein Recht auf Berichtigung,
          Sperrung oder L&ouml;schung dieser Daten. Diesbez&uuml;glich und auch
          zu weiteren Fragen zum Thema personenbezogene Daten k&ouml;nnen Sie
          sich jederzeit &uuml;ber die im Impressum aufgef&uuml;hrten
          Kontaktm&ouml;glichkeiten an uns wenden.
          <br />
          <strong>SSL- bzw. TLS-Verschl&uuml;sselung</strong>
          <br />
          Aus Sicherheitsgr&uuml;nden und zum Schutz der &Uuml;bertragung
          vertraulicher Inhalte, die Sie an uns als Seitenbetreiber senden,
          nutzt unsere Website eine SSL-bzw. TLS-Verschl&uuml;sselung. Damit
          sind Daten, die Sie &uuml;ber diese Website &uuml;bermitteln, f&uuml;r
          Dritte nicht mitlesbar. Sie erkennen eine verschl&uuml;sselte
          Verbindung an der &bdquo;https://&ldquo; Adresszeile Ihres Browsers
          und am Schloss-Symbol in der Browserzeile.
          <br />
          <strong>Kontaktformular</strong>
          <br />
          Per Kontaktformular &uuml;bermittelte Daten werden
          einschlie&szlig;lich Ihrer Kontaktdaten gespeichert, um Ihre Anfrage
          bearbeiten zu k&ouml;nnen oder um f&uuml;r Anschlussfragen
          bereitzustehen. Eine Weitergabe dieser Daten findet ohne Ihre
          Einwilligung nicht statt.
          <br />
          Die Verarbeitung der in das Kontaktformular eingegebenen Daten erfolgt
          ausschlie&szlig;lich auf Grundlage Ihrer Einwilligung (Art. 6 Abs. 1
          lit. a DSGVO). Ein Widerruf Ihrer bereits erteilten Einwilligung ist
          jederzeit m&ouml;glich. F&uuml;r den Widerruf gen&uuml;gt eine
          formlose Mitteilung per E-Mail. Die Rechtm&auml;&szlig;igkeit der bis
          zum Widerruf erfolgten Datenverarbeitungsvorg&auml;nge bleibt vom
          Widerruf unber&uuml;hrt.
          <br />
          &Uuml;ber das Kontaktformular &uuml;bermittelte Daten verbleiben bei
          uns, bis Sie uns zur L&ouml;schung auffordern, Ihre Einwilligung zur
          Speicherung widerrufen oder keine Notwendigkeit der Datenspeicherung
          mehr besteht. Zwingende gesetzliche Bestimmungen &ndash; insbesondere
          Aufbewahrungsfristen &ndash; bleiben unber&uuml;hrt.
          <br />
          <strong>Cookies</strong>
          <br />
          Unsere Website verwendet Cookies. Das sind kleine Textdateien, die Ihr
          Webbrowser auf Ihrem Endger&auml;t speichert. Cookies helfen uns
          dabei, unser Angebot nutzerfreundlicher, effektiver und sicherer zu
          machen.
          <br />
          Einige Cookies sind &ldquo;Session-Cookies.&rdquo; Solche Cookies
          werden nach Ende Ihrer Browser-Sitzung von selbst gel&ouml;scht.
          Hingegen bleiben andere Cookies auf Ihrem Endger&auml;t bestehen, bis
          Sie diese selbst l&ouml;schen. Solche Cookies helfen uns, Sie bei
          R&uuml;ckkehr auf unserer Website wiederzuerkennen.
          <br />
          Mit einem modernen Webbrowser k&ouml;nnen Sie das Setzen von Cookies
          &uuml;berwachen, einschr&auml;nken oder unterbinden. Viele Webbrowser
          lassen sich so konfigurieren, dass Cookies mit dem Schlie&szlig;en des
          Programms von selbst gel&ouml;scht werden. Die Deaktivierung von
          Cookies kann eine eingeschr&auml;nkte Funktionalit&auml;t unserer
          Website zur Folge haben.
          <br />
          Das Setzen von Cookies, die zur Aus&uuml;bung elektronischer
          Kommunikationsvorg&auml;nge oder der Bereitstellung bestimmter, von
          Ihnen erw&uuml;nschter Funktionen (z.B. Warenkorb) notwendig sind,
          erfolgt auf Grundlage von Art. 6 Abs. 1 lit. f DSGVO. Als Betreiber
          dieser Website haben wir ein berechtigtes Interesse an der Speicherung
          von Cookies zur technisch fehlerfreien und reibungslosen
          Bereitstellung unserer Dienste. Sofern die Setzung anderer Cookies
          (z.B. f&uuml;r Analyse-Funktionen) erfolgt, werden diese in dieser
          Datenschutzerkl&auml;rung separat behandelt.
          <br />
          <small>
            Quelle: Datenschutz-Konfigurator von&nbsp;
            <a
              href="http://www.mein-datenschutzbeauftragter.de/"
              target="_blank"
              rel="noreferrer noopener"
            >
              mein-datenschutzbeauftragter.de
            </a>
          </small>
          <br />
          <br />
          Die verantwortliche Stelle entscheidet allein oder gemeinsam mit
          anderen &uuml;ber die Zwecke und Mittel der Verarbeitung von
          personenbezogenen Daten (z.B. Namen, Kontaktdaten o. &Auml;.).
          <br />
          <strong>Widerruf Ihrer Einwilligung zur Datenverarbeitung</strong>
          <br />
          Nur mit Ihrer ausdr&uuml;cklichen Einwilligung sind einige
          Vorg&auml;nge der Datenverarbeitung m&ouml;glich. Ein Widerruf Ihrer
          bereits erteilten Einwilligung ist jederzeit m&ouml;glich. F&uuml;r
          den Widerruf gen&uuml;gt eine formlose Mitteilung per E-Mail. Die
          Rechtm&auml;&szlig;igkeit der bis zum Widerruf erfolgten
          Datenverarbeitung bleibt vom Widerruf unber&uuml;hrt.
          <br />
          <strong>
            Recht auf Beschwerde bei der zust&auml;ndigen Aufsichtsbeh&ouml;rde
          </strong>
          <br />
          Als Betroffener steht Ihnen im Falle eines datenschutzrechtlichen
          Versto&szlig;es ein Beschwerderecht bei der zust&auml;ndigen
          Aufsichtsbeh&ouml;rde zu. Zust&auml;ndige Aufsichtsbeh&ouml;rde
          bez&uuml;glich datenschutzrechtlicher Fragen ist der
          Landesdatenschutzbeauftragte des Bundeslandes, in dem sich der Sitz
          unseres Unternehmens befindet. Der folgende Link stellt eine Liste der
          Datenschutzbeauftragten sowie deren Kontaktdaten bereit:&nbsp;
          <a
            href="https://www.bfdi.bund.de/DE/Infothek/Anschriften_Links/anschriften_links-node.html"
            target="_blank"
            rel="noreferrer noopener"
          >
            https://www.bfdi.bund.de/DE/Infothek/Anschriften_Links/anschriften_links-node.html
          </a>
          .<br />
          <strong>Recht auf Daten&uuml;bertragbarkeit</strong>
          <br />
          Ihnen steht das Recht zu, Daten, die wir auf Grundlage Ihrer
          Einwilligung oder in Erf&uuml;llung eines Vertrags automatisiert
          verarbeiten, an sich oder an Dritte aush&auml;ndigen zu lassen. Die
          Bereitstellung erfolgt in einem maschinenlesbaren Format. Sofern Sie
          die direkte &Uuml;bertragung der Daten an einen anderen
          Verantwortlichen verlangen, erfolgt dies nur, soweit es technisch
          machbar ist.
          <br />
          <strong>
            Recht auf Auskunft, Berichtigung, Sperrung, L&ouml;schung
          </strong>
          <br />
          Sie haben jederzeit im Rahmen der geltenden gesetzlichen Bestimmungen
          das Recht auf unentgeltliche Auskunft &uuml;ber Ihre gespeicherten
          personenbezogenen Daten, Herkunft der Daten, deren Empf&auml;nger und
          den Zweck der Datenverarbeitung und ggf. ein Recht auf Berichtigung,
          Sperrung oder L&ouml;schung dieser Daten. Diesbez&uuml;glich und auch
          zu weiteren Fragen zum Thema personenbezogene Daten k&ouml;nnen Sie
          sich jederzeit &uuml;ber die im Impressum aufgef&uuml;hrten
          Kontaktm&ouml;glichkeiten an uns wenden.
          <br />
          <strong>SSL- bzw. TLS-Verschl&uuml;sselung</strong>
          <br />
          Aus Sicherheitsgr&uuml;nden und zum Schutz der &Uuml;bertragung
          vertraulicher Inhalte, die Sie an uns als Seitenbetreiber senden,
          nutzt unsere Website eine SSL-bzw. TLS-Verschl&uuml;sselung. Damit
          sind Daten, die Sie &uuml;ber diese Website &uuml;bermitteln, f&uuml;r
          Dritte nicht mitlesbar. Sie erkennen eine verschl&uuml;sselte
          Verbindung an der &bdquo;https://&ldquo; Adresszeile Ihres Browsers
          und am Schloss-Symbol in der Browserzeile.
          <br />
          <strong>Kontaktformular</strong>
          <br />
          Per Kontaktformular &uuml;bermittelte Daten werden
          einschlie&szlig;lich Ihrer Kontaktdaten gespeichert, um Ihre Anfrage
          bearbeiten zu k&ouml;nnen oder um f&uuml;r Anschlussfragen
          bereitzustehen. Eine Weitergabe dieser Daten findet ohne Ihre
          Einwilligung nicht statt.
          <br />
          Die Verarbeitung der in das Kontaktformular eingegebenen Daten erfolgt
          ausschlie&szlig;lich auf Grundlage Ihrer Einwilligung (Art. 6 Abs. 1
          lit. a DSGVO). Ein Widerruf Ihrer bereits erteilten Einwilligung ist
          jederzeit m&ouml;glich. F&uuml;r den Widerruf gen&uuml;gt eine
          formlose Mitteilung per E-Mail. Die Rechtm&auml;&szlig;igkeit der bis
          zum Widerruf erfolgten Datenverarbeitungsvorg&auml;nge bleibt vom
          Widerruf unber&uuml;hrt.
          <br />
          &Uuml;ber das Kontaktformular &uuml;bermittelte Daten verbleiben bei
          uns, bis Sie uns zur L&ouml;schung auffordern, Ihre Einwilligung zur
          Speicherung widerrufen oder keine Notwendigkeit der Datenspeicherung
          mehr besteht. Zwingende gesetzliche Bestimmungen &ndash; insbesondere
          Aufbewahrungsfristen &ndash; bleiben unber&uuml;hrt.
          <br />
          <strong>Cookies</strong>
          <br />
          Unsere Website verwendet Cookies. Das sind kleine Textdateien, die Ihr
          Webbrowser auf Ihrem Endger&auml;t speichert. Cookies helfen uns
          dabei, unser Angebot nutzerfreundlicher, effektiver und sicherer zu
          machen.
          <br />
          Einige Cookies sind &ldquo;Session-Cookies.&rdquo; Solche Cookies
          werden nach Ende Ihrer Browser-Sitzung von selbst gel&ouml;scht.
          Hingegen bleiben andere Cookies auf Ihrem Endger&auml;t bestehen, bis
          Sie diese selbst l&ouml;schen. Solche Cookies helfen uns, Sie bei
          R&uuml;ckkehr auf unserer Website wiederzuerkennen.
          <br />
          Mit einem modernen Webbrowser k&ouml;nnen Sie das Setzen von Cookies
          &uuml;berwachen, einschr&auml;nken oder unterbinden. Viele Webbrowser
          lassen sich so konfigurieren, dass Cookies mit dem Schlie&szlig;en des
          Programms von selbst gel&ouml;scht werden. Die Deaktivierung von
          Cookies kann eine eingeschr&auml;nkte Funktionalit&auml;t unserer
          Website zur Folge haben.
          <br />
          Das Setzen von Cookies, die zur Aus&uuml;bung elektronischer
          Kommunikationsvorg&auml;nge oder der Bereitstellung bestimmter, von
          Ihnen erw&uuml;nschter Funktionen (z.B. Warenkorb) notwendig sind,
          erfolgt auf Grundlage von Art. 6 Abs. 1 lit. f DSGVO. Als Betreiber
          dieser Website haben wir ein berechtigtes Interesse an der Speicherung
          von Cookies zur technisch fehlerfreien und reibungslosen
          Bereitstellung unserer Dienste. Sofern die Setzung anderer Cookies
          (z.B. f&uuml;r Analyse-Funktionen) erfolgt, werden diese in dieser
          Datenschutzerkl&auml;rung separat behandelt.
          <br />
          <small>
            Quelle: Datenschutz-Konfigurator von&nbsp;
            <a
              href="http://www.mein-datenschutzbeauftragter.de/"
              target="_blank"
              rel="noreferrer noopener"
            >
              mein-datenschutzbeauftragter.de
            </a>
          </small>
        </p>
      </div>
    </Layout>
  );
};

export default Datenschutz;
