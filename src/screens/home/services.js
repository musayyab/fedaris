import React from "react";
import styled from "styled-components";
import ServiceCard from "../../components/serviceCard";
import { device } from "../../utility/layout";
import servicebgimg from "../../assets/images/servicesbg.png";
import theme from "../../utility/theme";
import CText from "../../components/text";
import { fontFamily, fSize } from "../../utility/font";
import MobileIcon from "../../assets/icons/mobileapp";
import DesignThinking from "../../assets/icons/designThinking";
import OnlineShoping from "../../assets/icons/onlineshopping";
import Consulting from "../../assets/icons/consultation";

const MainWrapper = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  background-image: url(${servicebgimg});
  background-repeat: no-repeat;
  background-size: cover;
`;
const MWShadow = styled.div`
  width: 100%;
  /* height : 100%; */
  background-color: ${theme.darkshade};
  display: flex;
  justify-content: center;
  align-items: center;
  padding-top: 50px;
  padding-bottom: 50px;
`;
const SubWrapper = styled.div`
  width: 80%;
  max-width: ${device.desktopL};
  display: flex;
  flex-direction: column;
  flex-wrap: wrap;
  justify-content: space-around;
  align-items: center;
  /* @media ${device.mobileS} { 
        flex-direction : column;
        width : 90%;
    }
    @media ${device.laptop} { 
        flex-direction : row;
        width : 80%;
    } */
`;
const TitleWrapper = styled.div`
  margin-bottom: 20px;
`;

const DescWrapper = styled.div`
  text-align: center;
  margin-bottom: 30px;
`;
const SWCards = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  justify-content: center;
  gap: 30px;
`;

const FlexRow = styled.div`
  display: flex;
  flex-direction: row;
`;

const ServicesPage = () => {
  return (
    <MainWrapper>
      <MWShadow>
        <SubWrapper>
          <TitleWrapper>
            <CText
              label="Unsere "
              color={theme.white}
              ffamily={fontFamily("semiB")}
              fsize={fSize("35px")}
            />
            <CText
              label="Leistungen "
              color={theme.maincolor}
              ffamily={fontFamily("semiB")}
              fsize={fSize("35px")}
            />
          </TitleWrapper>
          <DescWrapper>
            <CText
              label="Unsere Leistungen umfassen die Entwicklung von App-, Web- und Softwarelösungen. Dabei verfolgen wir einen ganzheitlichen Ansatz und stehen Ihnen von der Entwicklung über die Umsetzung bis hin zum Launch und darüber hinaus als kompetente Partner zur Seite."
              color={theme.gray1}
              ffamily={fontFamily("small")}
              fsize={fSize("regular")}
              textAlign="center"
            />
          </DescWrapper>
          <SWCards>
            <ServiceCard
              icon={<MobileIcon />}
              label="App Entwicklung"
              description="Wir entwerfen, entwickeln und launchen individuelle mobile Apps für Smartphones, Tablets und Desktop Ansicht. "
            />
            <ServiceCard
              icon={<DesignThinking />}
              label="Webentwicklung"
              description="Fedaris verfügt über langjährige Erfahrung im Bereich der Erstellung moderner und professioneller Websites und -anwendungen zur Optimierung Ihrer Internetpräsenz."
            />
            <ServiceCard
              icon={<OnlineShoping />}
              label="E-Commerce"
              description="Sie wollen Ihre Produkte online verkaufen? Wir entwickeln maßgeschneiderte Online-Shops und sorgen für ein unvergleichbares Shopping-Erlebnis Ihrer Zielgruppe."
            />
            <ServiceCard
              icon={<OnlineShoping />}
              label="Softwareentwicklung"
              description="Auch bei klassischen Entwicklungsprojekten stehen wir Ihnen stets zur Seite und bringen den digitalen Reifegrad Ihres Unternehmens auf ein neues Level. "
            />
            <ServiceCard
              icon={<Consulting />}
              label="Beratung"
              description="Sie haben noch keine Idee? Gemeinsam entwickeln wir Konzepte, um die Prozesse Ihres Unternehmens zu optimieren und die Produktivität zu steigern. "
            />
            <ServiceCard
              icon={<Consulting />}
              label="Service & Support"
              description="Vor dem Launch ist nach dem Launch! Egal ob Wartung oder Erweiterung des Produkts – Wir stehen Ihnen stets als Ansprechpartner für Ihre Anliegen zur Seite"
            />
          </SWCards>
        </SubWrapper>
      </MWShadow>
    </MainWrapper>
  );
};

export default ServicesPage;
