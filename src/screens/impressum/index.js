import React, { useState } from "react";

import styled from "styled-components";
import Layout from "../layout/index";

const Impressum = () => {
  return (
    <Layout>
      <div
        style={{
          paddingLeft: "10%",
          paddingRight: "10%",
          marginBottom: 10,
        }}
      >
        <strong>Impressum</strong>
        <p>&nbsp;</p>
        <p>Angaben gem&auml;&szlig; &sect; 5 TMG</p>
        <p>&nbsp;</p>
        <p>Fedaris GbR</p>
        <p>Werrestra&szlig;e 53</p>
        <p>32049 Herford</p>
        <p>&nbsp;</p>
        <p>Vertreten durch den Gesch&auml;ftsf&uuml;hrer:</p>
        <p>Ahmad Ismail Tahir</p>
        <p>&nbsp;</p>
        <strong>Kontakt</strong>
        <p>Telefon: +4915155769891</p>
        <p>E-Mail: info@fedaris.de</p>
      </div>
    </Layout>
  );
};

export default Impressum;
