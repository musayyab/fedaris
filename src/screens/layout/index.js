import React, { useState } from "react";
import Header from "../../components/header";

import Footer from "../home/footer";

import styled from "styled-components";
import { device } from "../../utility/layout";
import theme from "../../utility/theme";
import Logo from "../../assets/icons/ninj.svg";

import Hamburger from "../../assets/icons/hamburger";
import LinkButton from "../../components/linkButton";

import LabelIcon from "../../components/labelIcon";
import Mail from "../../assets/icons/mail";
import Pin from "../../assets/icons/pin";
import PhoneIcon from "../../assets/icons/phone";

const MobHeader = styled.div`
  margin-bottom: 20px;
  padding-left: 20px;
  padding-right: 20px;
  background-color: ${theme.darkshade};
  padding-top: 20px;
  padding-bottom: 15px;
  @media ${device.mobileS} {
    display: block;
  }
  @media ${device.mobileM} {
    display: block;
  }
  @media ${device.mobileL} {
    display: block;
  }
  @media ${device.tablet} {
    display: none;
  } ;
`;
const WebHeader = styled.div`
  margin-bottom: 20px;
  @media ${device.mobileS} {
    display: none;
  }
  @media ${device.mobileM} {
    display: none;
  }
  @media ${device.mobileL} {
    display: none;
  }
  @media ${device.tablet} {
    display: block;
  } ;
`;

const RespMob = styled.div`
  height: ${(props) => (props.height ? "380px" : "50px")};
  transition: all 0.3s ease-out;
  overflow: hidden;
`;
const Layout = (props) => {
  const [resHeight, setHeight] = useState(false);

  const toggle = () => {
    resHeight ? setHeight(false) : setHeight(true);
  };

  return (
    <div>
      <MobHeader>
        <RespMob height={resHeight}>
          <div style={{ marginBottom: 20 }}>
            <span onClick={toggle}>
              <Hamburger fill={theme.white} />
            </span>
            <a href="">
              <img src={Logo} alt="logo" style={{ marginLeft: 15 }} />
            </a>
          </div>
          <div style={{ paddingLeft: 60, marginBottom: 6 }}>
            <LinkButton label="Home" href="/" color={theme.white} />
          </div>
          <div style={{ paddingLeft: 60, marginBottom: 6 }}>
            <LinkButton
              label="Impressum"
              href="/impressum"
              color={theme.white}
            />
          </div>
          <div style={{ paddingLeft: 60, marginBottom: 6 }}>
            <LinkButton
              label="Datenschutz"
              href="/datenschutz"
              color={theme.white}
            />
          </div>

          {/* <div style={{ paddingLeft: 60, marginBottom: 10 }}>
            <Button label="Get Started" />
          </div> */}
          <div style={{ paddingLeft: 60, marginBottom: 10 }}>
            <a href="tel:+4915155769891">
              <LabelIcon icon={<PhoneIcon />} text="+4915155769891" />
            </a>
          </div>
          <div style={{ paddingLeft: 60, marginBottom: 10 }}>
            <a href="mailto:info@fedaris.de">
              <LabelIcon icon={<Mail />} text="info@fedaris.de" />
            </a>
          </div>
          <div style={{ paddingLeft: 60, marginBottom: 10 }}>
            <LabelIcon icon={<Pin />} text="Deutschland" />
          </div>
          {/* <div
            style={{
              paddingLeft: 60,
              marginBottom: 10,
              display: "flex",
              flexDirection: "row",
            }}
          >
            <span style={{ marginRight: 15 }}>
              <Facebook />
            </span>
            <Instagram />
          </div> */}
        </RespMob>
      </MobHeader>
      <WebHeader>
        <Header />
      </WebHeader>
      {props.children}
      <div>
        <Footer />
      </div>
    </div>
  );
};

export default Layout;
