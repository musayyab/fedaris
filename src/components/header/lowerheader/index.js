import React from "react";
import styled from "styled-components";
import LinkButton from "../../linkButton";
import Logo from "../../../assets/icons/ninj.svg";
import Button from "../../button";

const MainWrapper = styled.div`
  display: flex;
  align-items: center;
`;
const SubWrapper = styled.div`
  max-width: 1440px;
  width: 100%;
  display: flex;
  justify-content: space-between;
`;
const LeftWrapper = styled.div`
  padding-right: 15px;
  padding-top: 15px;
  padding-bottom: 15px;
  flex-direction: row;
  display: flex;
  gap: 15px;
`;
const RightWrapper = styled.div`
  padding: 15px;
  padding-right: 0px;
  flex-direction: row;
  display: flex;
  gap: 15px;
  align-items: center;
`;

const LowerHeader = () => {
  return (
    <MainWrapper>
      <SubWrapper>
        <LeftWrapper>
          <a href="/">
            <img src={Logo} alt="logo" style={{ marginLeft: 0 }} />
          </a>
        </LeftWrapper>
        <RightWrapper>
          <LinkButton label="Home" href="/" />
          <LinkButton label="Impressum" href="/impressum" />
          <LinkButton label="Datenschutz" href="/datenschutz" />
        </RightWrapper>
      </SubWrapper>
    </MainWrapper>
  );
};

export default LowerHeader;

// LinkButton
